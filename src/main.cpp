#include <Arduino.h>
#include <M5Unified.hpp>

#if defined(ARDUINO_M5Stick_C) || defined(ARDUINO_M5Stick_C_Plus)
enum Pin {
    PWM = GPIO_NUM_32,
    DIR = GPIO_NUM_33
};
#else
// M5 Atom S3
enum Pin {
    PWM = GPIO_NUM_2,
    DIR = GPIO_NUM_1
};
#endif

const unsigned int PWM_CH = 0;
const unsigned int PWM_FREQ = 12000;   // 12 kHz
const unsigned int PWM_RESOLUTION = 8; // 8 bit resolution [0-255]

enum Mode {
    STANDBY = 0,
    FORWARD_25 = 1,
    FORWARD_50 = 2,
    FORWARD_75 = 3,
    FORWARD_100 = 4
} mode;

const char* MODE_NAMES[] = {
    "STBY",
    "F25",
    "F50",
    "F75",
    "F100"
};

void printMode(Mode mode) {
    M5.Display.setTextSize(2);
    M5.Display.setCursor(0, 0);
    M5.Display.printf("M: %4s", MODE_NAMES[mode]);
}

void setup() {
    auto config = M5.config();
    M5.begin(config);
    pinMode(Pin::DIR, OUTPUT);
    digitalWrite(Pin::DIR, HIGH);

    pinMode(Pin::PWM, OUTPUT);
    ledcSetup(PWM_CH, PWM_FREQ, PWM_RESOLUTION);
    ledcAttachPin(Pin::PWM, PWM_CH);
    ledcWrite(PWM_CH, 0);

    mode = STANDBY;
    printMode(mode);
}

void loop() {
    M5.update();
    if (M5.BtnA.wasReleased()) {
        switch (mode) {
            case Mode::STANDBY:
                mode = FORWARD_25;
                ledcWrite(PWM_CH, 63);
                break;
            case Mode::FORWARD_25:
                mode = FORWARD_50;
                ledcWrite(PWM_CH, 127);
                break;
            case Mode::FORWARD_50:
                mode = FORWARD_75;
                ledcWrite(PWM_CH, 191);
                break;
            case Mode::FORWARD_75:
                mode = FORWARD_100;
                ledcWrite(PWM_CH, 255);
                break;
            case Mode::FORWARD_100:
                ledcWrite(PWM_CH, 0);
                mode = STANDBY;
                break;
        }
        printMode(mode);
    }
    delay(10);
}